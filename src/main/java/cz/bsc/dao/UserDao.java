package cz.bsc.dao;

import cz.bsc.domain.User;

import java.util.List;

public interface UserDao {

    List<User> getAllUsers();

}
