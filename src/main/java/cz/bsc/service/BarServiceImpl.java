package cz.bsc.service;

import cz.bsc.dao.UserDao;
import cz.bsc.domain.User;

import java.util.List;

public class BarServiceImpl {

    private UserDao userDao;

    public int serveBeerForAll() {

        List<User> users = userDao.getAllUsers();

        for (User user : users) {
            if (user.getAge() < 18) {
                throw new IllegalStateException("User " + user.getName() + "is to young.");
            }
        }

        return users.size();

    }

}
